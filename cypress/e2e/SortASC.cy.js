describe('Login', () => {
  it('passes', () => {
    cy.visit('https://www.saucedemo.com/');
    cy.get('[data-test=username]').type('standard_user');
    cy.get('[data-test=password]').type('secret_sauce');
    cy.get('.login_wrapper').click();
    cy.get('[data-test=login-button]').click();

    //cy.xpath("//input[@id=\'user-name\']").type("standard_user");
    //cy.get("#password").click();
    //cy.xpath(" //input[@id=\'password\']").type("secret_sauce");
    //cy.get('[data-test=login-button]').click();
    //cy.get('*[data-test="login-button"]').click();

    // Get the list of prices before filtering
   let beforeFilterPriceList = [];

    cy.get('.inventory_item_price').each(($price) => {
    const priceText = $price.text().replace('$', '');
    beforeFilterPriceList.push(parseFloat(priceText));
    }).then(() => {
    // Sort the list of prices
    beforeFilterPriceList = beforeFilterPriceList.sort();

    // Filter the prices using a dropdown selection
    cy.get('.product_sort_container').select('Price (low to high)');

    // Get the list of prices after filtering
    let afterFilterPriceList = [];

    cy.get('.inventory_item_price').each(($price) => {
    const priceText = $price.text().replace('$', '');
    afterFilterPriceList.push(parseFloat(priceText));
    }).then(() => {
    // Sort the list of prices
    afterFilterPriceList = afterFilterPriceList.sort();

    // Assert that the filtered prices match the original prices
    expect(beforeFilterPriceList).to.deep.equal(afterFilterPriceList);
    });
   });

    //Captur the price using the element
    //cy.get('.inventory_item_price').each((item, index) =>{
      //cy.wrap(item)
    //})
    //const beforeFilterPriceList = [];

// Assume 'p' is a reference to the element containing the price text
//const priceText = p.text().replace('$', '');
//beforeFilterPriceList.push(parseFloat(priceText));


  })
})