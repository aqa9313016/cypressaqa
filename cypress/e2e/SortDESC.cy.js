describe('template spec', () => {
  it('passes', () => {
    cy.visit('https://www.saucedemo.com/')
    cy.get('[data-test=username]').type('standard_user');
    cy.get('[data-test=password]').type('secret_sauce');
    cy.get('.login_wrapper').click();
    cy.get('[data-test=login-button]').click();

    //Change the sorting to Name ( Z -> A).
    cy.xpath ("//div[@id=\'header_container\']/div[2]/div/span/select").click();

//before filter capture the prices
const beforeFilterPrice = cy.get('.inventory_item_price');

//remove the $symbol from the price and convert the string into double
const beforeFilterPriceList = [];
beforeFilterPrice.each(($el) => {
beforeFilterPriceList.push(parseFloat($el.text().replace('$', '')));
});

//filter the price from the drop down
cy.get('.product_sort_container').select('Price (high to low)');

//After filter capture the prices
const afterFilterPrice = cy.get('.inventory_item_price');

// remove $ symbol from the price and convert the string into double
const afterFilterPriceList = [];
afterFilterPrice.each(($el) => {
afterFilterPriceList.push(parseFloat($el.text().replace('$', '')));
});

//Compare the values/Assert the values(first we need to sort the values of before filterPrice)
beforeFilterPriceList.sort().reverse();
expect(beforeFilterPriceList).to.deep.equal(afterFilterPriceList);


  })

  

})